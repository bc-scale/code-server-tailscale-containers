# Code-Server-Tailscale-Containers
This repository should encapsulate all of our efforts in getting `code-server` containers working on various container runtime platforms.

## Support
We currently support:
1. **CSTail Ubuntu/Fedora Container Images (Images)**: Docker images that we publish to gitlab for code-server. Come with `code-server` and `tailscale` installed, but have no container command or entrypoint, so you will have to write your own `code-server`/`tailscale` commands to get your container working.
2. **CSTail Helm Chart for Kubernetes Deployment(K8s/Helm)**: [Chart repo](https://gitlab.com/api/v4/projects/38930595/packages/helm/stable). [Containers registry](https://gitlab.com/ac306/code-server-tailscale-containers/container_registry).

## Description
### Images
We build images and scan them with gitlab's SAST scanning tools. These images have everything you need to run `tailscale` and `code-server`, but do not do anything special (i.e. have no entrypoints/container commands).

### K8s/Helm
We currently support running `code-server` containers in Kubernetes clusters using our helm chart. Pull down the chart by adding the `ac306` helm stable channel to your helm repo list:
```sh
$ helm repo add ac306 https://gitlab.com/api/v4/projects/38930595/packages/helm/stable
```
and create a values file for yourself, and deploy:
```sh
$ helm install <release_name> (--dry-run) --create-namespace --namespace <deployment_namespace> --values path/to/your/values-file.yaml ac306/cstail
```
The charts values are fairly well documented in the base `values.yaml` file, so all the descriptions of the chart values should be done there.

The architecture of the system is fairly simple:
- **Init Containers**: Perform `tailscale` check to ensure that the machine hostname has not been claimed. If it has, it will assume that another deployment hasn't cleaned up after itself, and will wait for 30 seconds for prestop container lifecycle hooks from an old deployment to execute. If after 30 seconds the hostname still hasn't disappeared, the init container will manually delete the machine on `tailscale` itself.
- **Tailscale Sidecar Container**: The `tailscale` sidecar runs `tailscaled`, which is shared with the `code-server` container as well. The sidecar registers the pod with your `tailnet` and bootstraps the certificate generation components.
- **Code-Server Container**: This generates a TLS certificate for `code-server` if required, then runs code server.
- **Pre-Stop Hook**: Hook to cleanup a stopped/failed pod machine id in `tailscale`.

After everything is running, you should be able to check the logs of the `code-server-` container for the URL to your `code-server` instance. Connect to it via your favorite `tailscale` supported device. No services, ingress, or load-balancers required.

## Contributing
Please please please bump the helm chart version when you make changes to the helm chart. We follow [Semantic Versioniong](https://semver.org/) - so change the versions of the chart according to the summary doc.

## FAQ
**Q**: My code-server is taking forever to start! It was working perfectly before but it has gotten really slow now! I see logs that seem to be stuck at generating certificates. What's going on? \
**A**: You may have made too many requests to generate TLS certificates, and are being rate-limited by Tailscale/LetsEncrypt. Please make sure you are persisting the tailscale certifictes somewhere, and re-using them.

**Q**: [K8s/Helm] I created a deployment with the helm chart, but for some reason it hasn't started up correctly. The certificate filenames look wonky, and I see something about tailscale version != something something. What does this mean? \
**A**: This means that `tailscale` was unable to grab the `tailscale` domain name for your machine. This could be either because you haven't enabled magic-dns, or your api-keys to tailscale aren't working.

**Q**: [K8s/Helm] I tried to increate the replica count but everything is broken idk whats happening! Help? \
**A**: At this time, we only support running a single replica of the deployment. Please do not scale up the replica set, that doesn't do what you may think it does. Please uninstall your helm release, and redeploy fresh.
