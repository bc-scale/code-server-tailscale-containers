{{/*
Expand the name of the chart.
*/}}
{{- define "code-server-tailscale-ubuntu.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "code-server-tailscale-ubuntu.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "code-server-tailscale-ubuntu.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "code-server-tailscale-ubuntu.labels" -}}
helm.sh/chart: {{ include "code-server-tailscale-ubuntu.chart" . }}
{{ include "code-server-tailscale-ubuntu.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "code-server-tailscale-ubuntu.selectorLabels" -}}
app.kubernetes.io/name: {{ include "code-server-tailscale-ubuntu.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "code-server-tailscale-ubuntu.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "code-server-tailscale-ubuntu.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the secret
*/}}
{{- define "code-server-tailscale-ubuntu.secretName" -}}
{{- if .Values.secret.create }}
{{- default (include "code-server-tailscale-ubuntu.fullname" .) .Values.secret.name }}
{{- else }}
{{- default "tailscale-secret" .Values.secret.name }}
{{- end }}
{{- end }}

{{/*
Create the name of the machines hostname on the tailnet
*/}}
{{- define "code-server-tailscale-ubuntu.tailnetHostname" -}}
{{- if .Values.tailnetConfig.tailnetHostnameOverride }}
{{- .Values.tailnetConfig.tailnetHostnameOverride | trunc 63 | trimSuffix "-" }}
{{- else if .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{ include "code-server-tailscale-ubuntu.fullname" . }}
{{- end }}
{{- end }}
